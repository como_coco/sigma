﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp6.CadastroUsuariosistema
{
    public class DTO
    {
        public string Nome { get; set; }
        public string Usuario { get; set; }
        public string Perfil { get; set; }
        public string Senha { get; set; }
        public string ConfirmarSenha { get; set; }
        public string Departamento { get; set; }
    }
}
