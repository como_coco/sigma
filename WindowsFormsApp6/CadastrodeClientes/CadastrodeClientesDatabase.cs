﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp6.CadastrodeClientes
{
    public class CadastrodeClientesDatabase
    {
        public int Cadastrar(CadastrodeClientesDTO dto)
        {
            string script = @" INSERT INTO tb_ () VALUES (@)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_categoria", dto.NomeCategoria));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public List<CadastrodeClientesDTO> Listar()
        {
            string script = "select * from ";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CadastrodeclientesDTO> listar = new List<CadastrodeClientesDTO>();

            while (reader.Read())
            {
                CadastrodeClientesDTO dto = new CadastrodeClientesDTO();
                dto. = reader.GetInt32("");
                dto. = reader.GetString("");

                listar.Add(dto);
            }
            reader.Close();
            return listar;
        }
    }
}
