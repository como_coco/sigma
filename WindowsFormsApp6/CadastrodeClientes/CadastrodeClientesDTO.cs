﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp6.CadastrodeClientes
{
    public class CadastrodeClientesDTO
    {
        public string Nome { get; set; }
        public int CPF { get; set; }
        public string Profissão { get; set; }
        public string DatadeNascimento { get; set; }
        public string CliendeDesde { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public int Numero { get; set; }
        public string Referencia { get; set; }
        public int CEP { get; set; }
        public string Referencia { get; set; }
        public string Complemento { get; set; }
        public int Telefone { get; set; }
        public int TelefoneComercial { get; set; }
        public string Email { get; set; }
        public string Ramal { get; set; }
    }
}
