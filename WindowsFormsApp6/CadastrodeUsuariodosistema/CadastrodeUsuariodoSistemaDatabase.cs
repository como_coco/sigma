﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp6.CadastrodeUsuariodosistema
{
   public class CadastrodeUsuariodoSistemaDatabase
    {
        public int Cadastrar(CadastrodeUsuariodoSistemaDTO dto)
        {
            string script = @" INSERT INTO tb_ (nm_) VALUES (@nm_)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_", dto.NomeCategoria));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public List<CadastrodeUsuariodoSistemaDTO> Listar()
        {
            string script = "select * ";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CadastrodeclientesDTO> listar = new List<CadastrodeClientesDTO>();

            while (reader.Read())
            {
                CadastrodeUsuariodoSistemaDTO dto = new CadastrodeUsuariodoSistemaDTO();
                dto.ID = reader.GetInt32("ID_");
                dto. = reader.GetString("nm_");

                listar.Add(dto);
            }
            reader.Close();
            return listar;
        }
    }
}
