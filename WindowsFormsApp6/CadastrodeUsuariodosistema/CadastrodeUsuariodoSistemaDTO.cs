﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp6.CadastrodeUsuariodosistema
{
    public class CadastrodeUsuariodoSistemaDTO
    {
        public string nome { get; set; }
        public string Usuario { get; set; }
        public string Perfil { get; set; }
        public string Senha { get; set; }
        public string confirmarSenha { get; set; }
    }
}
